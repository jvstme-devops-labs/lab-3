# Lab 3

Docker Compose project to run a sample Python application using nginx web server and SQLite database.

## Run

```shell
docker compose up
```

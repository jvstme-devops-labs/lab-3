from datetime import datetime

from fastapi import Depends, FastAPI

from db import DB, Increment


app = FastAPI()


@app.get("/api/counter")
def get_counter(db: DB) -> int:
    return db.query(Increment).count()


@app.get("/api/increment")
def increment_counter(db: DB) -> int:
    increment = Increment(date=datetime.now())
    db.add(increment)
    db.commit()

    return get_counter(db)
